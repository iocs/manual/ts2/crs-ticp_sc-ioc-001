###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                    EH - Heater
##                                                                                          ##  
##                                                                                          ##  
############################ Version: 1.0             ########################################
# Author:  Miklos Boros 
# Date:    19-05-2020
# Version: v1.0



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_Auto",             PV_DESC="Operation Mode Auto",    PV_ONAM="True",                    PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced",  PV_ONAM="True",                    PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",    PV_ONAM="InhibitManual",           PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",     PV_ONAM="InhibitForce",            PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",        PV_ONAM="InhibitLocking",          PV_ZNAM="AllowLocking")
add_analog("HeaterColor",              "INT",                            PV_DESC="Heater color")

#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Free Run Button", PV_ONAM="True",                    PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",    PV_ONAM="True",                    PV_ZNAM="False")

#Measured value
add_digital("Heater_Status",           PV_DESC="Heater Status",             PV_ONAM="True",                    PV_ZNAM="False")
add_digital("Heater_Control",          PV_DESC="Heater Control",            PV_ONAM="True",                    PV_ZNAM="False")

#Interlocking
add_digital("GroupInterlock",          PV_DESC="GroupInterlock",         PV_ONAM="True",                    PV_ZNAM="False")
add_digital("StartInterlock",          PV_DESC="StartInterlock",         PV_ONAM="True",                    PV_ZNAM="False")
add_digital("StopInterlock",           PV_DESC="StopInterlock",          PV_ONAM="True",                    PV_ZNAM="False")



#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device Locked",          PV_ONAM="True",                    PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                           PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                           PV_DESC="Guest Lock ID")

#Alarm signals
add_major_alarm("LatchAlarm",          "Latching of alarms",             PV_ZNAM="True")
add_major_alarm("GroupAlarm",          "GroupAlarm",                     PV_ZNAM="NominalState")
add_major_alarm("TurnOn_TimeOut",      "Turn On Time Out",               PV_ZNAM="NominalState")
add_major_alarm("TurnOff_TimeOut",     "Turn Off Time Out",              PV_ZNAM="NominalState")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",          PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "HW Output Module Error",         PV_ZNAM="NominalState")

#OPI timeouts
add_time("StartingTime",               PV_DESC="Starting Time")
add_time("StoppingTime",               PV_DESC="Stopping Time")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")
add_digital("Cmd_ForceVal",            PV_DESC="CMD: Force Value")
add_digital("Cmd_ManuOpen",            PV_DESC="CMD: Manual Open")
add_digital("Cmd_ManuClose",           PV_DESC="CMD: Manual Close")
add_digital("Cmd_ForceOpen",           PV_DESC="CMD: Force Open")
add_digital("Cmd_ForceClose",          PV_DESC="CMD: Force Close")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()


#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                           PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                           PV_DESC="Device ID after Blockicon Open")