###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ##
##  CCDB device types: ICS_xxxxx                                                            ##
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##
#                          TT - Temperature Sensors (TE, TT, TR)                            ##
##                                                                                          ##
##                                                                                          ##
#############################         Version: 1.3             ###############################
# Author:  Emilio Asensi
# Date:    04-05-2021
# Version: v1.3
# Changes:
# 1. Added alarms
############################           Version: 1.2           ################################
# Author:  Miklos Boros
# Date:    27-05-2019
# Version: v1.2
# Changes:
# 1. Variable Name Unification
############################           Version: 1.1           ################################
# Author:	Emilio Asensi
# Date:	13-09-2018
# Version:  v1.1
# Changes:
#Accommodate the changes on CMS_TT
# 1. Modified Alarm Signal section to be compatible with new format.
############################           Version: 1.0           ################################
# Author:   Miklos Boros, Emilio Asensi
# Date:	    23-07-2018
# Version:  v1.0
# Note:     Based on CMS_PT.def


############################
#  STATUS BLOCK
############################
define_status_block()

#Operation modes
add_digital("OpMode_FreeRun",                        PV_DESC="Operation Mode FreeRun",     PV_ONAM="True",             PV_ZNAM="False")
add_digital("OpMode_Forced",                         PV_DESC="Operation Mode Forced",      PV_ONAM="True",             PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Force",                         PV_DESC="Inhibit Force Mode",         PV_ONAM="InhibitForce",     PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",                          PV_DESC="Inhibit Locking",            PV_ONAM="InhibitLocking",   PV_ZNAM="AllowLocking")
add_analog("TransmitterColor",         "INT",        PV_DESC="Transmitter color")

#for OPI visualization
add_digital("EnableFreeRunBtn",                      PV_DESC="EnableFreeRunButton",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("EnableForcedBtn",                       PV_DESC="EnableForceButton",          PV_ONAM="True",             PV_ZNAM="False")
add_analog("ScaleLOW","REAL",                        PV_DESC="Scale LOW",                  PV_EGU="K")
add_analog("ScaleHIGH","REAL",                       PV_DESC="Scale HIGH",                 PV_EGU="K")

#Transmitter value
add_analog("MeasValue","REAL",                     PV_DESC="Temperature value",          PV_EGU="K")
add_analog("Resistance","REAL",                      PV_DESC="Resistance value",           PV_EGU="Ohm")
add_analog("Current","REAL",                         PV_DESC="Raw current value",          PV_EGU="mA")
add_analog("RAWValue",                 "REAL",  ARCHIVE=True,                           PV_DESC="RAW integer scaled" )

#Range selection
add_digital("RangeBit0",                             PV_DESC="First Range bit",            PV_ONAM="OK",               PV_ZNAM="NOK")
add_digital("RangeBit1",                             PV_DESC="Second Range bit",           PV_ONAM="OK",               PV_ZNAM="NOK")
add_analog("Range","INT",                            PV_DESC="Temperature enumeration range")
add_analog("RangeVoltage","REAL",                    PV_DESC="Range voltage value",        PV_EGU="V")

#Locking mechanism
add_digital("DevLocked",                             PV_DESC="Device locked",              PV_ONAM="True",             PV_ZNAM="False")
add_analog("Faceplate_LockID","DINT",                PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID","DINT",                PV_DESC="Guest Lock ID")

add_digital("LatchAlarm",                            PV_DESC="Latching of the alarms")
add_digital("GroupAlarm",                            PV_DESC="Group Alarm for OPI")

#Alarm signals
add_major_alarm("Underrange","Temperature Underrange",      PV_ZNAM="NominalState")
add_major_alarm("Overrange","Temperature Overrange",        PV_ZNAM="NominalState")
add_major_alarm("HIHI","Temperature HIHI",                  PV_ZNAM="NominalState")
add_minor_alarm("HI","Temperature HI",                      PV_ZNAM="NominalState")
add_minor_alarm("LO","Temperature LO",                      PV_ZNAM="NominalState")
add_major_alarm("LOLO","Temperature LOLO",                  PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "IO_Error",                       PV_ZNAM="NominalState")
add_major_alarm("DigitalInp_IO_Error","DigitalInp_IO_Error",PV_ZNAM="NominalState")
add_major_alarm("AnalogInp_IO_Error","AnalogInp_IO_Error",  PV_ZNAM="NominalState")
add_major_alarm("Module_Error","Module_Error",              PV_ZNAM="NominalState")
add_major_alarm("Param_Error",         "Parameter_Error",                PV_ZNAM="NominalState")

#Feedback
add_analog("FB_ForceValue","REAL",                   PV_DESC="Feedback Force temperature", PV_EGU="K")
add_analog("FB_Limit_HIHI","REAL",                   PV_DESC="Feedback Limit HIHI",        PV_EGU="K")
add_analog("FB_Limit_HI","REAL",                     PV_DESC="Feedback Limit HI",          PV_EGU="K")
add_analog("FB_Limit_LO","REAL",                     PV_DESC="Feedback Limit LO",          PV_EGU="K")
add_analog("FB_Limit_LOLO","REAL",                   PV_DESC="Feedback Limit LOLO",        PV_EGU="K")
add_analog("FB_PlugNumber","DINT",                   PV_DESC="Cernox plug number")
add_string("FB_SerialNumber", 16,                    PV_NAME="FB_SerialNumber",            PV_DESC="Cernox serial number")
add_string("FB_DeviceName", 16,                      PV_NAME="FB_DeviceName",              PV_DESC="Cernox device name")

############################
#  COMMAND BLOCK
############################
define_command_block()

#OPI buttons
add_digital("Cmd_FreeRun",                           PV_DESC="CMD: FreeRun Mode")
add_digital("Cmd_Force",                             PV_DESC="CMD: Force Mode")
add_digital("Cmd_ForceVal",                          PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",                          PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceUnlock",                       PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",                           PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",                         PV_DESC="CMD: Unlock Device")

############################
#  PARAMETER BLOCK
############################
define_parameter_block()

#Limits
add_analog("P_Limit_HIHI","REAL",                    PV_DESC="Limit HIHI",                  PV_EGU="K")
add_analog("P_Limit_HI","REAL",                      PV_DESC="Limit HI",                    PV_EGU="K")
add_analog("P_Limit_LO","REAL",                      PV_DESC="Limit LO",                    PV_EGU="K")
add_analog("P_Limit_LOLO","REAL",                    PV_DESC="Limit LOLO",                  PV_EGU="K")

#Calibration
add_analog("P_Calibration_n","DINT",                    PV_DESC="Calibration curve number")

#Forcing
add_analog("P_ForceValue","REAL",                    PV_DESC="Force temperature",           PV_EGU="K")

#Locking mechanism
add_analog("P_Faceplate_LockID","DINT",              PV_DESC="Device ID after lock")
add_analog("P_BlockIcon_LockID","DINT",              PV_DESC="Device ID after blockicon open")
